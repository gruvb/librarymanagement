package com.cognitree.gaurav;

import com.cognitree.gaurav.db.DBConnections;

import java.io.IOException;
import java.sql.SQLException;

public class TestConnection {
    public static void main(String[] args) throws IOException, SQLException {
        DBConnections DBConnections = new DBConnections();
        if(DBConnections.checkConnection() == Boolean.TRUE) {
            System.out.println("Connection Established");
        } else {
            System.out.println("Connection Broken");
        }

    }
}
