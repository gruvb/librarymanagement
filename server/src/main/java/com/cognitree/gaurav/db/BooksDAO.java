package com.cognitree.gaurav.db;

import javax.ws.rs.QueryParam;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface BooksDAO {
    public ResultSet getAllBooks() throws SQLException;
    public int borrowBooks(String bookName) throws SQLException;
    public int returnBooks(String bookName) throws SQLException;
}
