package com.cognitree.gaurav.db;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class BooksDAOImpl implements BooksDAO {

    public BooksDAOImpl() throws IOException, SQLException {
    }

    DBConnections DBConnections = new DBConnections();

    Statement statement = DBConnections.statement;
    @Override
    public ResultSet getAllBooks() throws SQLException {
        ResultSet resultSet = statement.executeQuery("Select * from Books");
        return resultSet;
    }

    @Override
    public int borrowBooks(String bookName) throws SQLException {
        int executed = statement.executeUpdate("UPDATE `LibraryManagement`.`Books` set " +
                "copies=copies-1 WHERE `title`= " + "'" + bookName + "';");
        return executed;
    }

    @Override
    public int returnBooks(String bookName) throws SQLException {
        int executed = statement.executeUpdate("UPDATE `LibraryManagement`.`Books` set " +
                "copies=copies+1 WHERE `title`= " + "'" + bookName + "';");
        return executed;

    }
}
