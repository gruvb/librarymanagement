package com.cognitree.gaurav.db;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DBConnections {

    private static final Logger logger = LogManager.getLogger(DBConnections.class);

    public DBConnections() throws IOException, SQLException {
    }

    private Statement createConnectionAndStatement() throws SQLException, IOException {
        FileInputStream fileInputStream = new FileInputStream("/Users/gc/Downloads/LibraryManagement" +
                "/server/src/main/resources/config.properties");
        Properties properties = new Properties();
        properties.load(fileInputStream);
        String user = properties.getProperty("db.user");
        String password = properties.getProperty("db.password");
        String url = properties.getProperty("db.url");
        Connection connection = DriverManager.getConnection("jdbc:mysql://"
                + url +"/LibraryManagement", user, password);
        return connection.createStatement();
    }

    Statement statement = createConnectionAndStatement();

    public Boolean checkConnection() throws SQLException {
        if (statement.isClosed()) {
            logger.debug("Connection Closed");
            return Boolean.FALSE;
        } else {
            logger.debug("Connection Started");
            return Boolean.TRUE;
        }
    }

}
