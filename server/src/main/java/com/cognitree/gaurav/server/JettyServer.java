package com.cognitree.gaurav.server;

import com.cognitree.gaurav.server.LibraryManagerResources;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class JettyServer
{
    public static void main( String[] args ) throws Exception {
        Server server = new Server(8080);
        server.setHandler(getJerseyHandler());
        server.start();
        server.join();
    }

    private static Handler getJerseyHandler() {
        ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        servletContextHandler.setContextPath("/");
        ServletHolder servletHolder = servletContextHandler.addServlet(ServletContainer.class, "/api/*");
        servletHolder.setInitParameter("jersey.config.server.provider.classnames", LibraryManagerResources.class.getCanonicalName());
        return servletContextHandler;
    }
}
