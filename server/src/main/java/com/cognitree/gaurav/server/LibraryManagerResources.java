package com.cognitree.gaurav.server;

import com.cognitree.gaurav.db.BooksDAO;
import com.cognitree.gaurav.db.BooksDAOImpl;
import com.cognitree.gaurav.model.Books;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;

import javax.ws.rs.core.MediaType;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/*
Endpoints-
GET http://localhost:8080/api/library/books - returns a list of books in db
POST http://localhost:8080/api/library/return?book=HarryPotter1 - returns books and adds count in db
POST http://localhost:8080/api/library/borrow?book=HarryPotter1 - borrows books and subtracts count in db
 */

@Path("/library")
public class LibraryManagerResources {

    private static final Logger logger = LogManager.getLogger(LibraryManagerResources.class);
    BooksDAO booksDAO = new BooksDAOImpl();

    public LibraryManagerResources() throws IOException, SQLException {
    }

    @Path("books")
    @Produces("application/json")
    @GET
    public List<Books> getAllBooks () throws SQLException, IOException {
        ResultSet resultSet = booksDAO.getAllBooks();
        List<Books> returnBooks = new ArrayList<>();
        while (resultSet.next()) {
            Books books = new Books();
            books.setTitle(resultSet.getString("title"));
            books.setAuthor(resultSet.getString("author"));
            books.setGenre(resultSet.getString("genre"));
            books.setCopies(resultSet.getInt("copies"));
            returnBooks.add(books);
        }
        logger.debug("Displayed All Books");
        return returnBooks;
    }

    @POST
    @Consumes("application/json")
    @Path("borrow")
    public void borrowBooks(@QueryParam("book") String bookName) throws SQLException, IOException {
        int check = booksDAO.borrowBooks(bookName);
        if(check>0) {
            logger.debug("executed query");
        } else {
            logger.debug("query not executed.");
        }
        logger.debug("Borrowed successfully");
    }

    @POST @Consumes("application/json")
    @Path("return")
    public void returnBooks(@QueryParam("book") String bookName) throws SQLException, IOException {
        int check = booksDAO.returnBooks(bookName);
        if(check>0) {
            logger.debug("executed query");
        } else {
            logger.debug("query not executed.");
        }
        logger.debug("Returned successfully");
    }

}
